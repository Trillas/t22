package UD22MVC.MySqlEjercicio1;

import java.sql.SQLException;

import MVC.MVC_Actividad2.view.InsertarCliente;
import MVC.MVC_Actividad2.view.ModificarCliente;
import MVC.MVC_Actividad2.view.VentanaPrincipal;
import UD22MVC.MySqlEjercicio1Controller.ClienteController;
import UD22MVC.MySqlEjercicio1ModelConexion.Conexion;
import UD22MVC.MySqlEjercicio1ModelDao.Dao;
import UD22MVC.MySqlEjercicio1ModelDto.Cliente;

/**
 * Hello world!
 *
 */
public class App 
{
// Llamamos a todas los objetos	
	private static Cliente cliente;
	VentanaPrincipal ventanaPrincipal;
	ModificarCliente modificarCliente;
	InsertarCliente insertarCliente;
	ClienteController clienteController;
	
    public static void main( String[] args ) throws SQLException
    {
      App principal = new App();
      Dao.conectar();
      principal.iniciar();

  
 
      
    }
    
    
    
    
    private void iniciar() {
    	
    	// Iniciamos todos los objetos en un metodo par aque se inicien todos
    	ventanaPrincipal=new VentanaPrincipal();
    	insertarCliente = new InsertarCliente();
    	modificarCliente= new ModificarCliente();
    	clienteController = new ClienteController();
    	
    	ventanaPrincipal.setCoordinador(clienteController);
    	insertarCliente.setCoordinador(clienteController);
    	modificarCliente.setCoordinador(clienteController);
    	ventanaPrincipal.setCoordinador(clienteController);
    	
    	clienteController.setVentanaPrincipal(ventanaPrincipal);
    	clienteController.setInsertarCliente(insertarCliente);
    	clienteController.setModificarCliente(modificarCliente);
    	
    	
    	
    	
    	
    	
    	
    	
    	
    }
}
