package UD22MVC.MySqlEjercicio1Controller;

import java.sql.SQLException;

import MVC.MVC_Actividad2.view.InsertarCliente;
import MVC.MVC_Actividad2.view.ModificarCliente;
import MVC.MVC_Actividad2.view.VentanaPrincipal;
import UD22MVC.MySqlEjercicio1ModelDao.Dao;
import UD22MVC.MySqlEjercicio1ModelDto.Cliente;
import UD22MVC.MySqlEjercicio1ModelServicio.ClienteServ;

public class ClienteController {
// Declaramos los objetos para poder setearlos y getearlos 
	private ClienteServ clienteServ;
	private VentanaPrincipal ventanaPrincipal;
	private ModificarCliente modificarCliente;
	private InsertarCliente insertarCliente;
	public VentanaPrincipal getVentanaPrincipal() {
		return ventanaPrincipal;
	}
	public void setVentanaPrincipal(VentanaPrincipal ventanaPrincipal) {
		this.ventanaPrincipal = ventanaPrincipal;
	}
	public ModificarCliente getModificarCliente() {
		return modificarCliente;
	}
	public void setModificarCliente(ModificarCliente modificarCliente) {
		this.modificarCliente = modificarCliente;
	}
	public InsertarCliente getInsertarCliente() {
		return insertarCliente;
	}
	public void setInsertarCliente(InsertarCliente insertarCliente) {
		this.insertarCliente = insertarCliente;
	}
	// Hacemos que sean visible las dos ventaas 
	public void mostrarInsertarCliente() {
		insertarCliente.setVisible(true);
	}
	public void mostrarModificarCliente() {
		modificarCliente.setVisible(true);
	}
	public ClienteServ getClienteServ() {
		return clienteServ;
	}
	public void setClienteServ(ClienteServ clienteServ) {
		this.clienteServ = clienteServ;
	}

	
	
	
	// Llamamos a las funciones de las dao previamente que han pasado por las validaciones 
	public void insertarCliente(Cliente cliente) throws SQLException {
		ClienteServ.validarRegistro(cliente);
	}
	
	public static Cliente buscarCliente(int codigoCliente) throws SQLException {
		return  Dao.buscarCliente(codigoCliente);	
	}
	
	
	public static void modificarCliente(Cliente cliente) throws SQLException {
		ClienteServ.validarModificacion(cliente);
	}

	public static void eliminarCliente(String codigo) throws SQLException {
		Dao.eliminarCliente(codigo);
	}
	
	
	
	
}
