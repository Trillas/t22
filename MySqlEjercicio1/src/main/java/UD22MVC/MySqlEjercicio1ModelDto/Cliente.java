package UD22MVC.MySqlEjercicio1ModelDto;

public class Cliente {
	private  Integer idCliente;
	private  String nombreCliente;
	private String apellidoCliente;
	private  String direccionCliente;
	private  String dniCliente;
	private  String fechaCliente;
	
	// Hacemos setters y getters de cada uno de los valores que tenemos que introducir
	public  Integer getIdCliente() {
		return idCliente;
	}
	public  void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	public  String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public  String getApellidoCliente() {
		return apellidoCliente;
	}
	public void setApellidoCliente(String apellidoCliente) {
		this.apellidoCliente = apellidoCliente;
	}
	public  String getDireccionCliente() {
		return direccionCliente;
	}
	public void setDireccionCliente(String direccionCliente) {
		this.direccionCliente = direccionCliente;
	}
	public  String getDniCliente() {
		return dniCliente;
	}
	public void setDniCliente(String dniCliente) {
		this.dniCliente = dniCliente;
	}

	public  String getfechaCliente() {
		return fechaCliente;
	}
	public void setfechaCliente(String string) {
		this.fechaCliente = string;
	}
	
	
	
	
	
}
