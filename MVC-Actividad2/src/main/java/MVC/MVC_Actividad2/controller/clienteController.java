package MVC.MVC_Actividad2.controller;

import java.sql.SQLException;

import MVC.MVC_Actividad2.dto.cliente;
import MVC.MVC_Actividad2.controller.clienteController;
import MVC.MVC_Actividad2.dao.clienteDao;
import MVC.MVC_Actividad2.service.clienteServ;
import MVC.MVC_Actividad2.view.VentanaPrincipal;
import MVC.MVC_Actividad2.view.InsertarCliente;
import MVC.MVC_Actividad2.view.ModificarCliente;

public class clienteController {

	// Declaramos los objetos para poder setearlos y getearlos 
		private clienteServ clienteServ;
		private VentanaPrincipal ventanaPrincipal;
		private Modificarcliente modificarcliente;
		private Insertarcliente insertarcliente;
		public VentanaPrincipal getVentanaPrincipal() {
			return ventanaPrincipal;
		}
		public void setVentanaPrincipal(VentanaPrincipal ventanaPrincipal) {
			this.ventanaPrincipal = ventanaPrincipal;
		}
		public Modificarcliente getModificarcliente() {
			return modificarcliente;
		}
		public void setModificarcliente(Modificarcliente modificarcliente) {
			this.modificarcliente = modificarcliente;
		}
		public Insertarcliente getInsertarcliente() {
			return insertarcliente;
		}
		public void setInsertarcliente(Insertarcliente insertarcliente) {
			this.insertarcliente = insertarcliente;
		}
		// Hacemos que sean visible las dos ventaas 
		public void mostrarInsertarcliente() {
			insertarcliente.setVisible(true);
		}
		public void mostrarModificarcliente() {
			modificarcliente.setVisible(true);
		}
		public clienteServ getclienteServ() {
			return clienteServ;
		}
		public void setclienteServ(clienteServ clienteServ) {
			this.clienteServ = clienteServ;
		}

		
		
		
		// Llamamos a las funciones de las dao previamente que han pasado por las validaciones 
		public void insertarcliente(cliente cliente) throws SQLException {
			clienteServ.validarRegistro(cliente);
		}
		
		public static cliente buscarcliente(int codigocliente) throws SQLException {
			return  clienteDao.buscarcliente(codigocliente);	
		}
		
		
		public static void modificarcliente(cliente cliente) throws SQLException {
			clienteServ.validarModificacion(cliente);
		}

		public static void eliminarcliente(String codigo) throws SQLException {
			clienteDao.eliminarcliente(codigo);
		}
		
		
		}
}
