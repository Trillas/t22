package MVC.MVC_Actividad2.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import MVC.MVC_Actividad2.conexion.Conexion;
import MVC.MVC_Actividad2.dto.videos;

public class videosDao {
	public static Connection conexion;
	
	// Hacemos aqui tambien la conexion para que cada vez que se ejecute se asegure que se haga la conexion
	  public static void conectar() {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				 conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.14:3306?useTimezone=true&serverTimezone=UTC","alb","diplodocus1");
				System.out.println("Servidor conectado"); // Hacemos la conexion
			}catch(SQLException | ClassNotFoundException ex) {
				System.out.println("No se ha podido conectar con mi base de datos");
				System.out.println(ex);
			}
		}
	
	// Hacemos el metodo para insertar el ciente
	public static void insertarvideos(videos videos) throws SQLException
	{
	
		
		try {
			
			String Querydb = "USE "+"videos"+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String sql=  "INSERT INTO videos (id, titulo, director, cli_id) VALUE ('"+videos.getId()+"','"
					+videos.getTitulo()+"','"+videos.getDirector()+"','"
					+videos.getCli_id()+"');";
					
	
			Statement st = conexion.createStatement();
			st.executeUpdate(sql);
	
			JOptionPane.showMessageDialog(null, "Se ha introducido los datos correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);
			st.close();
	
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se han introducido los datos");
		}
	}

	
// Haemos el metodo para buscar el videos
	public static videos buscarvideos(int codigo) throws SQLException 
	{
		Conexion conex= new Conexion();
		videos videos= new videos();
		boolean existe=false;
		try {
			
			String Querydb = "USE "+"videos"+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String sql= "SELECT * FROM videos where id = ? ;";
			PreparedStatement consulta = conexion.prepareStatement(sql);
			consulta.setInt(1, codigo);
			ResultSet res = consulta.executeQuery();
			while(res.next()){
				existe=true;
				videos.setId(Integer.parseInt(res.getString("id")));
				videos.setTitulo(res.getString("titulo"));
				videos.setDirector(res.getString("director"));
				videos.setCli_id(res.getInt("cli_id"));
			 }
			res.close();
			conex.desconectar();
			System.out.println(sql);
					
			} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Error, no se conecto");
					System.out.println(e);
			}
		
			if (existe) {
				return videos;
			}
			else return null;				
	}

	// Tambien el update para modificar el videos
public static void modificarvideos(videos videos) throws SQLException {
		
		Conexion conex= new Conexion();
		try{
			String Querydb = "USE "+"videos"+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String consulta="UPDATE videos SET id= ? ,titulo = ? , director=? , cli_id=? ";
			PreparedStatement estatuto = conexion.prepareStatement(consulta);
			
            estatuto.setInt(1, videos.getId());
            estatuto.setString(2, videos.getTitulo());
            estatuto.setString(3, videos.getDirector());
            estatuto.setInt(4, videos.getCli_id());
            estatuto.executeUpdate();
            
          JOptionPane.showMessageDialog(null, " Se ha Modificado Correctamente ","Confirmación",JOptionPane.INFORMATION_MESSAGE);
          System.out.println(consulta);
         

        }catch(SQLException	 e){

            System.out.println(e);
            JOptionPane.showMessageDialog(null, "Error al Modificar","Error",JOptionPane.ERROR_MESSAGE);

        }
	}

// Y lo mismo para elminiar, haremos un delete 

public static void eliminarvideos(String codigo) throws SQLException
{
	Conexion conex= new Conexion();
	try {
		String Querydb = "USE "+"videos"+";";
		Statement stdb= conexion.createStatement();
		stdb.executeUpdate(Querydb);
		String sql= "DELETE FROM videos WHERE id='"+codigo+"'";
		Statement st = conexion.createStatement();
		st.executeUpdate(sql);
        JOptionPane.showMessageDialog(null, " Se ha Eliminado Correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
        System.out.println(sql);
		st.close();
		conex.desconectar();
		
	} catch (SQLException e) {
        System.out.println(e.getMessage());
		JOptionPane.showMessageDialog(null, "No se Elimino");
	}
}
}
