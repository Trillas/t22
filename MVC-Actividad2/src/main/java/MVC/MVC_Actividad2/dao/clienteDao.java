package MVC.MVC_Actividad2.dao;

import java.sql.*;

import javax.swing.JOptionPane;

import MVC.MVC_Actividad2.dto.*;
import MVC.MVC_Actividad2.conexion.*;

public class clienteDao {
	
	public static Connection conexion;
	
	// Hacemos aqui tambien la conexion para que cada vez que se ejecute se asegure que se haga la conexion
	  public static void conectar() {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				 conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.14:3306?useTimezone=true&serverTimezone=UTC","alb","diplodocus1");
				System.out.println("Servidor conectado"); // Hacemos la conexion
			}catch(SQLException | ClassNotFoundException ex) {
				System.out.println("No se ha podido conectar con mi base de datos");
				System.out.println(ex);
			}
		}
	
	// Hacemos el metodo para insertar el ciente
	public static void insertarCliente(cliente cliente) throws SQLException
	{
	
		
		try {
			
			String Querydb = "USE "+"Cliente"+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String sql=  "INSERT INTO cliente (id,nombre,apellido,direccion,dni,fecha) VALUE ('"+cliente.getId()+"','"
					+cliente.getNombre()+"','"+cliente.getApellido()+"','"
					+cliente.getDireccion()+"', '"+cliente.getDni()+"','"+cliente.getFecha()+"');";
					
	
			Statement st = conexion.createStatement();
			st.executeUpdate(sql);
	
			JOptionPane.showMessageDialog(null, "Se ha introducido los datos correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);
			st.close();
	
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se han introducido los datos");
		}
	}

	
// Haemos el metodo para buscar el cliente
	public static cliente buscarCliente(int codigo) throws SQLException 
	{
		Conexion conex= new Conexion();
		cliente cliente= new cliente();
		boolean existe=false;
		try {
			
			String Querydb = "USE "+"Cliente"+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String sql= "SELECT * FROM cliente where id = ? ;";
			PreparedStatement consulta = conexion.prepareStatement(sql);
			consulta.setInt(1, codigo);
			ResultSet res = consulta.executeQuery();
			while(res.next()){
				existe=true;
				cliente.setId(Integer.parseInt(res.getString("id")));
				cliente.setNombre(res.getString("nombre"));
				cliente.setApellido(res.getString("apellido"));
				cliente.setDireccion(res.getString("direccion"));
				cliente.setDni(res.getString("dni"));
				cliente.setFecha(res.getString("fecha"));
			 }
			res.close();
			conex.desconectar();
			System.out.println(sql);
					
			} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Error, no se conecto");
					System.out.println(e);
			}
		
			if (existe) {
				return cliente;
			}
			else return null;				
	}

	// Tambien el update para modificar el cliente
public static void modificarCliente(cliente cliente) throws SQLException {
		
		Conexion conex= new Conexion();
		try{
			String Querydb = "USE "+"Cliente"+";";
			Statement stdb= conexion.createStatement();
			stdb.executeUpdate(Querydb);
			String consulta="UPDATE cliente SET id= ? ,nombre = ? , apellido=? , direccion=? , dni= ? , fecha= ? WHERE id= ? ";
			PreparedStatement estatuto = conexion.prepareStatement(consulta);
			
            estatuto.setInt(1, cliente.getId());
            estatuto.setString(2, cliente.getNombre());
            estatuto.setString(3, cliente.getApellido());
            estatuto.setString(4, cliente.getDireccion());
            estatuto.setString(5,cliente.getDni());
            estatuto.setString(6, cliente.getFecha());
            estatuto.setInt(7, cliente.getId());
            estatuto.executeUpdate();
            
          JOptionPane.showMessageDialog(null, " Se ha Modificado Correctamente ","Confirmación",JOptionPane.INFORMATION_MESSAGE);
          System.out.println(consulta);
         

        }catch(SQLException	 e){

            System.out.println(e);
            JOptionPane.showMessageDialog(null, "Error al Modificar","Error",JOptionPane.ERROR_MESSAGE);

        }
	}

// Y lo mismo para elminiar, haremos un delete 

public static void eliminarCliente(String codigo) throws SQLException
{
	Conexion conex= new Conexion();
	try {
		String Querydb = "USE "+"Cliente"+";";
		Statement stdb= conexion.createStatement();
		stdb.executeUpdate(Querydb);
		String sql= "DELETE FROM cliente WHERE id='"+codigo+"'";
		Statement st = conexion.createStatement();
		st.executeUpdate(sql);
        JOptionPane.showMessageDialog(null, " Se ha Eliminado Correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
        System.out.println(sql);
		st.close();
		conex.desconectar();
		
	} catch (SQLException e) {
        System.out.println(e.getMessage());
		JOptionPane.showMessageDialog(null, "No se Elimino");
	}
}

}
