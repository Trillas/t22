package MVC.MVC_Actividad2.conexion;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Conexion {
	public static Connection conexion;
	   static String login = "alb"; // Ponemos nuestras credenciales
	   static String password = "diplodocus1";
	   static String url = "jdbc:mysql://192.168.1.14:3306?useTimezone=true&serverTimezone=UTC\",\"alb\",\"diplodocus1";

	   Connection conn = null;

	   // Construimos el conector para que pruebe a conectarse a la bd
	   public static void conectar() {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				 conexion = DriverManager.getConnection("jdbc:mysql://192.168.1.14:3306?useTimezone=true&serverTimezone=UTC","alb","diplodocus1");
				System.out.println("Servidor conectado"); // Hacemos la conexion
			}catch(SQLException | ClassNotFoundException ex) {
				System.out.println("No se ha podido conectar con mi base de datos");
				System.out.println(ex);
			}
		}
	   
	   //Permite retornar la conexion
	   public Connection getConnection(){
	      return conn;
	   }

	   public void desconectar(){
	      conn = null;
	   }
	   
	// Muestra la fecha
	 	public static void fecha() {
	 		Date date = new Date(0);
	 		DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
	 		System.out.println(" - " + hourdateFormat.format(date));
	 		}

	}
