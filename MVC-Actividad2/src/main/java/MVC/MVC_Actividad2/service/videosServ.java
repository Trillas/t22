package MVC.MVC_Actividad2.service;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import MVC.MVC_Actividad2.controller.videosController;
import MVC.MVC_Actividad2.dao.videosDao;
import MVC.MVC_Actividad2.dto.videos;

public class videosServ {
	private videosController videosController;
	public static boolean modificavideos=false;
	public void setvideosController(videosController videosController) {
		this.setvideosController(videosController);		
	}
// Haremos una validacion para que se tenga que poner mas de un caracter en el nombre y en el apellido 
	public static void validarRegistro(videos videos) throws SQLException {
		if (videos.getApellido().length() > 1 && videos.getNombre().length() > 1 ) {
			 videosDao.insertarvideos(videos);						
		}else {
			JOptionPane.showMessageDialog(null,"El nombre y el apellido deben tener mas de una letra","Advertencia",JOptionPane.WARNING_MESSAGE);
			
		}
		
	}
	
	
	
	public static void validarModificacion(videos videos) throws SQLException {
		videosDao dao;
		if (videos.getNombre().length()> 1 &&  videos.getApellido().length() > 1) {
			dao = new videosDao();
			videosDao.modificarvideos(videos);	
			modificavideos=true;
		}else{
			JOptionPane.showMessageDialog(null,"El nombrey el apellido deben tener mas de una letra","Advertencia",JOptionPane.WARNING_MESSAGE);
			modificavideos=false;
		}
	}
}
