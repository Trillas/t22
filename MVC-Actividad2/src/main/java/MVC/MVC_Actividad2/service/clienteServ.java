package MVC.MVC_Actividad2.service;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import MVC.MVC_Actividad2.controller.*; 
import MVC.MVC_Actividad2.dto.*; 
import MVC.MVC_Actividad2.dao.clienteDao;



public class clienteServ {

		private clienteController clienteController;
		public static boolean modificaCliente=false;
		public void setClienteController(clienteController ClienteController) {
			this.setClienteController(ClienteController);		
		}
	// Haremos una validacion para que se tenga que poner mas de un caracter en el nombre y en el apellido 
		public static void validarRegistro(cliente cliente) throws SQLException {
			if (cliente.getApellido().length() > 1 && cliente.getNombre().length() > 1 ) {
				 clienteDao.insertarCliente(cliente);						
			}else {
				JOptionPane.showMessageDialog(null,"El nombre y el apellido deben tener mas de una letra","Advertencia",JOptionPane.WARNING_MESSAGE);
				
			}
			
		}
		
		
		
		public static void validarModificacion(cliente cliente) throws SQLException {
			clienteDao dao;
			if (cliente.getNombre().length()> 1 &&  cliente.getApellido().length() > 1) {
				dao = new clienteDao();
				clienteDao.modificarCliente(cliente);	
				modificaCliente=true;
			}else{
				JOptionPane.showMessageDialog(null,"El nombrey el apellido deben tener mas de una letra","Advertencia",JOptionPane.WARNING_MESSAGE);
				modificaCliente=false;
			}
		}
}
