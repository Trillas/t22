package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ventanaPrincipal extends JFrame {

	private JPanel contentPane;

	public ventanaPrincipal() {
		setTitle("Principal");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 715, 275);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPrincipal = new JLabel("Que acción quieres realizar?");
		lblPrincipal.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPrincipal.setBounds(235, 53, 239, 31);
		contentPane.add(lblPrincipal);
		
		JButton btnBuscarClientes = new JButton("Buscar clientes");
		btnBuscarClientes.setBounds(25, 144, 117, 25);
		contentPane.add(btnBuscarClientes);
		
		JButton btnRegistrarClientes = new JButton("Registrar clientes");
		btnRegistrarClientes.setBounds(311, 144, 163, 25);
		contentPane.add(btnRegistrarClientes);
		
		JButton btnRegistrarVideos = new JButton("Registrar videos");
		btnRegistrarVideos.setBounds(519, 144, 141, 25);
		contentPane.add(btnRegistrarVideos);
		
		JButton btnBuscarVideos = new JButton("Buscar videos");
		btnBuscarVideos.setBounds(171, 144, 117, 25);
		contentPane.add(btnBuscarVideos);
	}
}
